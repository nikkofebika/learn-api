// $("#btn-search").on('click', function(){
// 	// dca61bcc
// 	var title = $('#search-input').val();
// 	console.log(title);
// 	$.get('http://www.omdbapi.com?apikey=289e33b3&s='+title, function(res){
// 		console.log(res);
// 		let movie = res.Search;
// 		if (res.Response == "True") {
// 			$.each(movie, function(i, v){

// 				$('#daftar-film').append(`
// 					<div class="col-md-4">
// 						<div class="card" style="width: 18rem;">
// 							<img src="`+ v.Poster +`" class="card-img-top" alt="...">
// 							<div class="card-body">
// 								<h5 class="card-title">`+ v.Title +`</h5>
// 								<p class="card-text">Type : `+ v.Type +`</p>
// 								<p class="card-text">Year : `+ v.Year +`</p>
// 								<h5 class="card-text">imdbID : `+ v.imdbID +`</h5>
// 								<a href="#" class="btn btn-primary">Pesan</a>
// 							</div>
// 						</div>
// 					</div>
// 					`);
// 			});
// 		}else{
// 			console.log(res.Error);
// 				$('#daftar-film').html(`
// 					<div class="col">
// 						<h2 class="text-center">`+ res.Error +`</h2>
// 					</div>
// 					`);
// 			}
// 		});
// });

$("#btn-search").on('click', function(){
	searchFilm();
});

$("#search-input").on('keyup', function(e){
	if (e.keyCode === 13) {
		searchFilm();
	}
});

function searchFilm(){
	$('#daftar-film').html('');
	// dca61bcc
	var title = $('#search-input').val();
	console.log(title);
	$.ajax({
		url : "http://www.omdbapi.com",
		type : 'get',
		dataType : 'json',
		data : {
			'apikey' : '289e33b3',
			's' : title
		},
		success : function(res){
			console.log(res);
			let movie = res.Search;
			if (res.Response == "True") {
				$.each(movie, function(i, v){

					$('#daftar-film').append(`
						<div class="col-md-4">
						<div class="card" style="width: 18rem;">
						<img src="`+ v.Poster +`" class="card-img-top" alt="...">
						<div class="card-body">
						<h5 class="card-title">`+ v.Title +`</h5>
						<p class="card-text">Type : `+ v.Type +`</p>
						<a href="#" class="btn-detail btn btn-primary btn-sm btn-block" data-id="`+ v.imdbID +`" data-toggle="modal" data-target="#mdl-detail-movie">Detail</a>
						</div>
						</div>
						</div>
						`);
				});
			}else{
				console.log(res.Error);
				$('#daftar-film').html(`
					<div class="col">
					<h2 class="text-center">`+ res.Error +`</h2>
					</div>
					`);
			}
		}
	});
}


$('#daftar-film').on('click', '.btn-detail', function(){
	const movieId = $(this).data('id');

	$.ajax({
		url : "http://www.omdbapi.com",
		type: 'get',
		dataType: 'json',
		data: {
			'apikey': '289e33b3',
			'i': movieId
		},
		success: function(result){
			$('.modal-title').text(result.Title);
			console.log(result);
			$('.modal-body').html(`
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<img src="`+ result.Poster +`" class="img-fluid">
						</div>
						<div class="col-md-8">
							<ul class="list-group">
								<li class="list-group-item">Actors : `+ result.Actors +`</li>
								<li class="list-group-item">Awards: `+ result.Awards +`</li>
								<li class="list-group-item">Box Office: `+ result.BoxOffice +`</li>
								<li class="list-group-item">Country: `+ result.Country +`</li>
								<li class="list-group-item">Genre: `+ result.Genre +`</li>
								<li class="list-group-item">Writer: `+ result.Writer +`</li>
								<li class="list-group-item">Year: `+ result.Year +`</li>
							</ul>
						</div>
					</div>
				</div>
				`);
		}
	});
});