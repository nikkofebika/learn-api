<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

require APPPATH . 'libraries/REST_Controller_Definitions.php';

class Mahasiswa extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Mahasiswa_model');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_get()
    {
        $id = $this->get('id');

        if ($id === null) {
            $mhs = $this->Mahasiswa_model->get();
        }else{
            $mhs = $this->Mahasiswa_model->get($id);
        }

        $this->set_response($mhs, 200);
    }

    public function index_post()
    {
        $data = [
            'nrp' => $this->post('nrp'),
            'nama' => $this->post('nama'),
            'email' => $this->post('email'),
            'jurusan' => $this->post('jurusan')
        ];

        if ($this->Mahasiswa_model->create($data) > 0) {
            $this->set_response(['status' => true, 'msg'=>"insert berhasil"], 201);
        }else{
            $this->set_response(['status' => false, 'msg'=>"insert gagal"], 400);

        }

    }

    public function index_put(){
        $id = $this->put('id');
        $data = [
            'nrp' => $this->put('nrp'),
            'nama' => $this->put('nama'),
            'email' => $this->put('email'),
            'jurusan' => $this->put('jurusan')
        ];

        if ($this->Mahasiswa_model->update($data, $id) > 0) {
            $this->set_response(['status' => true, 'msg'=>"update berhasil"], 201);
        }else{
            $this->set_response(['status' => false, 'msg'=>"update gagal"], 400);

        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        // Validate the id.
        if ($id === null)
        {
            // Set the response and exit
            $this->response("ID tidak ditemukan", 400); // BAD_REQUEST (400) being the HTTP response code
        }else{
            if ($this->Mahasiswa_model->delete($id) > 0) {
                $message = [
                    'id' => $id,
                    'message' => 'Deleted the resource'
                ];

                $this->set_response($message, 200); // NO_CONTENT (204) being the HTTP response code
            }else{
                $this->set_response('tidak ada ID tidak dapat dihapus', 400);
            }
        }


    }

}
